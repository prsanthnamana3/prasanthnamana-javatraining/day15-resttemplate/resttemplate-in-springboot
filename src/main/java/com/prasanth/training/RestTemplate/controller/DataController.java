package com.prasanth.training.RestTemplate.controller;

import com.prasanth.training.RestTemplate.pojos.Person;
import com.prasanth.training.RestTemplate.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/persons")
public class DataController {

    @Autowired
    PersonService personService;

    @RequestMapping(value="/getPersons", method = RequestMethod.GET)
    public List<Person> getPersons(){
        return personService.getPersons();
    }

}
