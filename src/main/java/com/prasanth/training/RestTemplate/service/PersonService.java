package com.prasanth.training.RestTemplate.service;

import com.prasanth.training.RestTemplate.pojos.Person;

import java.util.List;

public interface PersonService {
    List<Person> getPersons();
}
