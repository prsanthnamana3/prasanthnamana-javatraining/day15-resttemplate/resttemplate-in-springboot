package com.prasanth.training.RestTemplate.service;

import com.prasanth.training.RestTemplate.pojos.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {


    private final RestTemplate restTemplate = new RestTemplate();

    @Override
    public List<Person> getPersons() {
//        HttpHeaders headers = new HttpHeaders();
//        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//        HttpEntity<String> entity = new HttpEntity<String>(headers);
        return restTemplate.exchange("http://localhost:8080/p/getPersons", HttpMethod.GET, null, new ParameterizedTypeReference<List<Person>>(){}).getBody();
    }
}
